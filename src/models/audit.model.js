const { DataTypes } = require('sequelize');
const { sequelize } = require('../config/conn');

const Audit = sequelize.define('Audit', {
  uid: {
    field: 'UID',
    type: DataTypes.STRING,
    allowNull: false,
  },
  process: {
    field: 'PROCESS',
    type: DataTypes.STRING,
    allowNull: false,
  },
  requestObject: {
    field: 'REQUEST_OBJECT',
    type: DataTypes.STRING,
    allowNull: false,
  },
  startDate: {
    field: 'START_DATE',
    type: DataTypes.DATE,
    allowNull: false,
  }
},
  {
    timestamps: false,
    tableName: 'TK_TA_AUDIT',
  });

module.exports = Audit;


