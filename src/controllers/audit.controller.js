const { createAudit } = require("../services/audit.service");

const store = async (req, res) => {
  try {
    const data = await createAudit(req.body);
    return res.status(201).json({
      data: data
    })
  } catch (error) {
    return res.status(500).json({
      errorMessage: 'Error: Problemas al crear un audit',
      error
    })
  }
}

module.exports = {
  store
}