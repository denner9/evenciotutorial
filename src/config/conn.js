const Sequelize = require('sequelize');

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: 'mysql'
  }
);

function openConn() {
  sequelize.authenticate().then(() => {
    console.log('DATABASE CONNECTED')
  }).catch((error) => [
    console.error('DATABASE ERROR: ', error)
  ]);

  sequelize.sync().then(() => {
    console.log('DATABASE SYNC')
  }).catch((error) => [
    console.error('DATABASE SYNC ERROR: ', error)
  ]);
}

module.exports = {
  sequelize,
  openConn,
}