const { celebrate, Joi, Segments } = require('celebrate');

const storeValidation = celebrate({
  [Segments.BODY]: Joi.object().keys({
    uid: Joi.string().required(),
    process: Joi.string().required(),
    requestObject: Joi.string().required(),
    startDate: Joi.date().required(),
  }),
});

module.exports = {
  storeValidation,
}