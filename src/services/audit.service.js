const Audit = require('../models/audit.model');

const createAudit = async (data) => {

  const newAudit = {
    uid: data.uid,
    process: data.process,
    requestObject: data.requestObject,
    startDate: data.startDate,
  }

  const audit = await Audit.create(newAudit);
  return audit;
}

module.exports = {
  createAudit
}