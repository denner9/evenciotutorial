var express = require('express');
const { storeValidation } = require('../middlewares/audit.middleware');
const { store } = require('../controllers/audit.controller');
var router = express.Router();

router.post('/audit', storeValidation, store)

module.exports = router;
