require('dotenv').config();
const { openConn } = require('./src/config/conn')

var express = require('express');
var logger = require('morgan');

// Router
var auditRouter = require('./src/routes/audit');

var app = express();

app.use(logger('dev'));
app.use(express.json());

// Routes
app.use('', auditRouter);

const { PORT } = process.env;

// Main server
app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
  openConn();
})


module.exports = app;
